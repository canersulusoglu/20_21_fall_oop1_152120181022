#include <iostream>
#include <fstream>
using namespace std;


/**
* This function returns the sum of numbers of array.
* @param double *Numbers
* @param int countOfNumbers
*/
double Sum(double *Numbers, int countOfNumbers) {
	int i = 0;
	double Sum = 0;
	while (i < countOfNumbers) {
		Sum += Numbers[i];
		i++;
	}
	return Sum;
}

/**
* This function returns the product of numbers of array.
* @param double *Numbers
* @param int countOfNumbers
*/
double Product(double* Numbers, int countOfNumbers) {
	int i = 0;
	double Product = 1;
	while (i < countOfNumbers) {
		Product *= Numbers[i];
		i++;
	}
	return Product;
}

/**
* This function returns the average of numbers of array.
* @param double *Numbers
* @param int countOfNumbers
*/
double Average(double* Numbers, int countOfNumbers) {
	return Sum(Numbers, countOfNumbers) / (double)countOfNumbers;
}

/**
* This function returns the smallest numbers in the array.
* @param double *Numbers
* @param int countOfNumbers
*/
double Smallest(double* Numbers, int countOfNumbers) {
	int i = 1;
	double Smallest = Numbers[0];
	while (i < countOfNumbers) {
		if (Numbers[i] < Smallest) {
			Smallest = Numbers[i];
		}
		i++;
	}
	return Smallest;
}

void main() {
	while (true) {
		string fileName;
		cout << "Enter a file name: ";
		cin >> fileName;

		fstream InputFile(fileName, ios::in);
		if (InputFile.fail()) {
			cout << "Failed to open file! " << endl;
		}
		else {
			int countOfNumbers = 0;
			InputFile >> countOfNumbers; // Assign first line to variable;
			double* Numbers = new double[countOfNumbers];

			// Assign Numbers to array
			int i = 0;
			double temp;
			while (InputFile >> temp) {
				Numbers[i] = temp;
				i++;
			}

			// Print Results
			cout << "Sum: " << Sum(Numbers, countOfNumbers) << endl;
			cout << "Product: " << Product(Numbers, countOfNumbers) << endl;
			cout << "Average: " << Average(Numbers, countOfNumbers) << endl;
			cout << "Smallest: " << Smallest(Numbers, countOfNumbers) << endl;
		}
	}
}