#include <iostream>
#include <string>
#include <sstream>
#include <exception>
using namespace std;

/**
* This method is a exception class that is inherited of exception class.
*/
class BadLengthException : public exception {
private:
    int length;
public:
	/**
	* This method is a constructor that belongs the exception.
	* @param int l --> is a length of a word.
	*/
    BadLengthException(int l) {
        this->length = l;
    }
	
	
	/**
	* This method returns the what exception is.
	*/
    string what() throw () {       
        string str = to_string(this->length);
        return str;
    }
};


bool checkUsername(string username) {
	bool isValid = true;
	int n = username.length();
	if(n < 5) {
		throw BadLengthException(n);
	}
	for(int i = 0; i < n-1; i++) {
		if(username[i] == 'w' && username[i+1] == 'w') {
			isValid = false;
		}
	}
	return isValid;
}

int main() {
	int T; cin >> T;
	while(T--) {
		string username;
		cin >> username;
		try {
			bool isValid = checkUsername(username);
			if(isValid) {
				cout << "Valid" << '\n';
			} else {
				cout << "Invalid" << '\n';
			}
		} catch (BadLengthException e) {
			cout << "Too short: " << e.what() << '\n';
		}
	}
	return 0;
}