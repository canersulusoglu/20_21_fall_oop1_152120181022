#include <cstdio>
#include <iostream>
using namespace std;


int main() {
    int Count;
    while(1){
        cin >> Count;
        if(Count > 0){
            break;
        }
    }
    int *Arr = new int[Count]; 
    for(int i = 0; i < Count; i++){
        cin >> Arr[i];
    }
    for(int i = (Count-1); i >= 0; i--){
        cout << Arr[i] << " ";
    }
    delete[] Arr;
    
    return 0;
}