#include <iostream>
#include <cstdio>
using namespace std;

/**
* This function finds and returns the maximum number in four number.
* @param int a --> is a number.
* @param int b --> is a number.
* @param int c --> is a number.
* @param int d --> is a number.
*/
int max_of_four(int a, int b, int c, int d){
    int max = a;

    if(b > max){
        max = b;
    }
    if(c > max){
        max = c;
    }
    if(d > max){
        max = d;
    }
    
    return max;
}

int main() {
    int a, b, c, d;
    #pragma warning(suppress : 4996) // visual studio scanf disable warning
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);
    
    return 0;
}