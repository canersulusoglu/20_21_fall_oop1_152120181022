#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
using namespace std;

/**
* This function controls whether the tag has expected attribute.
* @param string Tag --> is a tag.
* @param string query --> is a attribute.
*/
bool tagControl(string Tag, string query) {
    //intvalue"BadVal" value
    bool result = false;
    for (int i = 0; i < query.length(); i++)
    {
        if (Tag[i] == query[i]) {
            result = true;
        }
        else {
            result = false;
        }
    }
    return result;
}

/**
* This function parse the html and found expected attribute.
* @param string Tags --> is a all Html tags.
* @param string Query --> is a query.
*/
string Parser(string* Tags, int n, string Query) {
    string Result;
    string CurrentTag;
    bool isFounded = false;
    for (int i = 0; i < n; i++) {
        bool isClosedTag = false;
        int tagStartIndex = Tags[i].find('<') + 1;
        int tagEndIndex = Tags[i].find(' ');
        string tagName = Tags[i].substr(tagStartIndex, tagEndIndex - tagStartIndex);
        (tagName.back() == '>') ? tagName.erase(tagName.length() - 1, 1) : tagName = tagName;
        (tagName[0] == '/') ? isClosedTag = true : isClosedTag = false;
        string tagAttrs = (tagEndIndex != -1) ? Tags[i].substr(tagEndIndex) : "";
        (tagAttrs.length() > 0 && tagAttrs.back() == '>') ? tagAttrs.erase(tagAttrs.length() - 1, 1) : tagAttrs = tagAttrs;

        if (isClosedTag) {// Eğer tagı kapatıyorsak
            if (CurrentTag.length() > 0) {
                if (CurrentTag.rfind('.') != -1) {
                    CurrentTag.erase(CurrentTag.rfind('.'));
                }
                else {
                    CurrentTag = "";
                }
            }
        }
        else {
            if (CurrentTag.length() > 0) {
                CurrentTag += '.' + tagName;
            }
            else {
                CurrentTag += tagName;
            }
        }

        string selectedTag = Query.substr(0, Query.find("~"));
        string selectedAttr = Query.substr(Query.find("~") + 1);

        if (CurrentTag == selectedTag) {
            if (tagAttrs.length() > 0) {
                while (tagAttrs.find(" = ") != -1) { // Delete ' = ' charachters
                    tagAttrs.erase(tagAttrs.find(" = "), 3);
                }
                tagAttrs.erase(0, 1); // Delete first space

                string attr;
                stringstream ssin(tagAttrs);
                while (ssin.good()) {
                    ssin >> attr;
                    if (tagControl(attr, selectedAttr)) {
                        Result = attr.substr(attr.find('"') + 1, attr.rfind('"') - attr.find('"') - 1);
                        isFounded = true;
                        break;
                    }
                }
                if (isFounded) {
                    break;
                }
            }
            else {
                isFounded = false;
            }
        }

    }
    if (isFounded) {
        return Result;
    }
    else {
        return "Not Found!";
    }
}

int main() {
    int n, q;
    while (1) {
        cin >> n >> q;
        if (n >= 1 && n <= 20 && q >= 1 && q <= 20) {
            break;
        }
    }
    // Tags
    string* Tags = new string[n];
    for (int i = 0; i < n; i++) {
        string temp;
        while (1) {
            getline(cin, temp);
            if (temp.length() <= 200 && temp.length() != 0) {
                break;
            }
        }
        Tags[i] = temp;
    }
    // Queries
    string* Queries = new string[q];
    for (int i = 0; i < q; i++) {
        string query;
        while (1) {
            getline(cin, query);
            if (query.length() <= 200 && query.length() != 0) {
                break;
            }
        }
        Queries[i] = query;
    }

    for (int i = 0; i < q; i++) {
        cout << Parser(Tags, n, Queries[i]) << endl;
    }

    return 0;
}