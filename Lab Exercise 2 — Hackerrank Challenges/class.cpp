#include <iostream>
#include <sstream>
#include <string>
using namespace std;

class Student{
    private:
        int age;
        int standard;
        string first_name;
        string last_name;
    public:
		/**
		* This method set age of the Student.
		* @param int val --> is a age.
		*/
        void set_age(int val){
            this->age = val;
        }
		/**
		* This method set standard of the Student.
		* @param int val --> is a standard.
		*/
        void set_standard(int val){
            this->standard = val;
        }
		/**
		* This method set first name of the Student.
		* @param string val --> is a first name.
		*/
        void set_first_name(string val){
            this->first_name = val;
        }
		/**
		* This method set last name of the Student.
		* @param string val --> is a last name.
		*/
        void set_last_name(string val){
            this->last_name = val;
        }
		/**
		* This method returns age of the Student.
		*/
        int get_age(){
            return this->age;
        }
		/**
		* This method returns standard of the Student.
		*/
        int get_standard(){
            return this->standard;
        }
		/**
		* This method returns fist name of the Student.
		*/
        string get_first_name(){
            return this->first_name;
        }
		/**
		* This method returns last name of the Student.
		*/
        string get_last_name(){
            return this->last_name;
        }
        /**
		* This method prints the Student.
		*/
        string to_string(){
            return std::to_string(this->age) + ',' + this->first_name + ',' + this->last_name + ',' + std::to_string(this->standard);
        }
};

int main() {
    int age, standard;
    string first_name, last_name;
    
    cin >> age >> first_name >> last_name >> standard;
    
    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);
    
    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();
    
    return 0;
}