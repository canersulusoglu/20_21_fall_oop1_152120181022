#include <stdio.h>

void update(int *a,int *b) {
    int tempa = *a;
    int tempb = *b;
    *a = tempa + tempb;
    *b = ((tempa - tempb) < 0) ? -(tempa - tempb) : (tempa - tempb);
}

int main() {
    int a, b;
    int *pa = &a, *pb = &b;
    
    #pragma warning(suppress : 4996) // visual studio scanf disable warning
    scanf("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}