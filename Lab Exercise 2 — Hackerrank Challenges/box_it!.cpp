//#include<bits/stdc++.h>
#include <iostream>

using namespace std;

class Box {
private:
    int length;
    int breadth;
    int height;
public:
	/**
	* This method is a empty constructor.
	*/
    Box() {
        this->length = 0;
        this->breadth = 0;
        this->height = 0;
    }
	/**
	* This method is a constructor that has parameters.
	* @param int l --> is a length of Box.
	* @param int b --> is a breadth of Box.
	* @param int h --> is a height of Box.
	*/
    Box(int l, int b, int h) {
        this->length = l;
        this->breadth = b;
        this->height = h;
    }
	/**
	* This method is a copy constructor.
	* @param Box* other --> is a another Box that copy to this Box.
	*/
    Box(Box* other) {
        this->breadth = other->breadth;
        this->height = other->height;
        this->length = other->length;
    }

	/**
	* This method returns length of Box.
	*/
    int getLength() {
        return this->length;
    }
	/**
	* This method returns breadth of Box.
	*/
    int getBreadth() {
        return this->breadth;
    }
	/**
	* This method returns height of Box.
	*/
    int getHeight() {
        return this->height;
    }

	/**
	* This method returns and calculates volume of Box.
	*/
    unsigned long long int CalculateVolume() {
        return (unsigned long long int)this->length * (unsigned long long int)this->breadth * (unsigned long long int)this->height;
    }

	/**
	* This operator overloading compares the Boxes.
	*/
    bool operator<(Box& b) {
        if (this->length < b.length) {
            return true;
        }
        else if (this->breadth < b.breadth && this->length == b.length) {
            return true;
        }
        else if (this->height < b.height && this->breadth == b.breadth && this->length == b.length) {
            return true;
        }
        else {
            return false;
        }
    }
};

/**
* This operator print the Box.
*/
ostream& operator<<(ostream& out, Box& B) {
    out << B.getLength() << ' ' << B.getBreadth() << ' ' << B.getHeight();
    return out;
}


void check2()
{
	int n;
	cin>>n;
	Box temp;
	for(int i=0;i<n;i++)
	{
		int type;
		cin>>type;
		if(type ==1)
		{
			cout<<temp<<endl;
		}
		if(type == 2)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			temp=NewBox;
			cout<<temp<<endl;
		}
		if(type==3)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			if(NewBox<temp)
			{
				cout<<"Lesser\n";
			}
			else
			{
				cout<<"Greater\n";
			}
		}
		if(type==4)
		{
			cout<<temp.CalculateVolume()<<endl;
		}
		if(type==5)
		{
			Box NewBox(temp);
			cout<<NewBox<<endl;
		}

	}
}

int main()
{
	check2();
}