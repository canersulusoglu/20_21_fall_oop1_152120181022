#include <cstdio>
#include <iostream>
using namespace std;


int main() {
    int n, q;
    cin >> n >> q;
    
    // Creating Arrays
    int** Array = new int*[n];
    for(int i = 0; i < n; i++){
        int ArraySize;
        cin >> ArraySize;
        Array[i] = new int[ArraySize];
        for(int j = 0; j < ArraySize; j++){
            cin >> Array[i][j];
        }
    }
    
    int index1, index2;
    for(int i = 0; i < q; i++){
        cin >> index1 >> index2;
        cout << Array[index1][index2] << endl;
    }
    
    // Deleting Arrays
    for (int i = 0; i < n; i++) {
        delete[] Array[i];
    }
    delete[] Array;
    
    return 0;
}