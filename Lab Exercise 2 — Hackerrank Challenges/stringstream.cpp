#include <sstream>
#include <vector>
#include <iostream>
#include <sstream>
using namespace std;

vector<int> parseInts(string str) {
	vector<int> numbers;
    stringstream ss(str);

    int num;
    char ch;
    while (ss.good()) {
        ss >> num >> ch;
        numbers.push_back(num);
    }
    return numbers;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for(int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }
    
    return 0;
}