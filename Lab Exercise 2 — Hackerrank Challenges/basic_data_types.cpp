#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    int a; // %d
    long int b; //%ld
    char c; // %c
    float d; // %f
    double e; // %lf
    /*
    Note: You can also use cin and cout instead of scanf and printf;
          however, if you are taking a million numbers as input and printing a million lines,
          it is faster to use scanf and printf.
    */
    #pragma warning(suppress : 4996) // visual studio scanf disable warning
    scanf("%d %ld %c %f %lf", &a, &b, &c, &d, &e);
    printf("%d\n%ld\n%c\n%f\n%lf\n", a, b, c, d, e);
    return 0;
}